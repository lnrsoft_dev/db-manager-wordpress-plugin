=== DB Manager by BestWebSoft ===
Contributors: bestwebsoft
Donate link: https://bestwebsoft.com/donate/
Tags: ajax, control acess to systems, BD Manager, database, DB manager, DB manager plugin, DB meneger, DB manadger, DB menager, DB managar, delete, delete Dumper, delete phpMyadmin, download, download Dumper, download phpMyadmin, Dumper, htaccess, htpasswd, mysql, phpMyadmin, update, update Dumper, update link, update phpMyadmin
Requires at least: 3.8
Tested up to: 4.7.4
Stable tag: 1.1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Manage database and file system right from your WordPress admin dashboard.

== Description ==

DB Manager plugin is a secured solution to download the latest version of PhpMyadmin or Dumper software. Control the access to your systems, download, and delete them.

Manage database and file system easily right from your WordPress admin dashboard!

http://www.youtube.com/watch?v=UFnLP9RGhEE

= Features =

* Automatically download latest versions of PhpMyadmin and Dumper
* Enable or disable access to PhpMyadmin and Dumper systems
* Compatible with latest WordPress version 
* Incredibly simple settings for fast setup without modifying code
* Detailed step-by-step documentation and videos

If you have a feature suggestion or idea you'd like to see in the plugin, we'd love to hear about it! [Suggest a Feature](https://support.bestwebsoft.com/hc/en-us/requests/new)

= Documentation & Videos =

* [[Doc] Installation](https://docs.google.com/document/d/1-hvn6WRvWnOqj5v5pLUk7Awyu87lq5B_dO-Tv-MC9JQ/edit)

= Help & Support =

Visit our Help Center if you have any questions, our friendly Support Team is happy to help — <https://support.bestwebsoft.com/>

= Translation =

* Russian (ru_RU)
* Ukrainian (uk)

Some of these translations are not complete. We are constantly adding new features which should be translated. If you would like to create your own language pack or update the existing one, you can send [the text of PO and MO files](http://codex.wordpress.org/Translating_WordPress) to [BestWebSoft](https://support.bestwebsoft.com/hc/en-us/requests/new) and we'll add it to the plugin. You can download the latest version of the program for work with PO and MO [files Poedit](http://www.poedit.net/download.php).

= Recommended Plugins =

* [Updater](https://bestwebsoft.com/products/wordpress/plugins/updater/?k=7a3dfcbfb6392ffe64c9be52b0901f14) - Automatically check and update WordPress core with all installed plugins to the latest versions. Manual mode, email notifications and backups of all your files and database before updating.

== Installation ==

1. Upload the `db-manager` folder to the `/wp-content/plugins/` directory.
2. Activate the plugin using the 'Plugins' menu in your WordPress admin pane.
3. You can adjust the necessary settings using your WordPress admin panel in "BWS Panel" > "DB Manager".

[View a Step-by-step Instruction on DB Manager Installation](https://docs.google.com/document/d/1mdj6buhgBpK7W1fTH6I6CZpR2KWpBbZMMUXDIU56r6g/)

== Frequently Asked Questions ==

= Why does the login and password input field appear when I try to go to PhpMyAdmin or Dumper, but if I try to enter my data it says that they are incorrect? =

It is the work of the browser's cache which is unfortunately not cleared by force (it is necessary to re-open the browser or try again later), this fact causes such an effect - cache loads the data, but in fact they are not the same. All you need is to click "Cancel" in the pop-up window and follow the link to the downloaded system of PhpMyAdmin or Dumper.

= What should I do if there is a message - "The plugins folder is not writable, please give permission!" =

You should grant permissions to read and write. See "Access rights to files and directories."

= How can I update PhpMyAdmin or Dumper? =

If you have a new version you'll see a link and you will be able to upgrade by clicking on the link.

= I have some problems with the plugin's work. What Information should I provide to receive proper support? =

Please make sure that the problem hasn't been discussed yet on our forum (<https://support.bestwebsoft.com>). If no, please provide the following data along with your problem's description:

1. the link to the page where the problem occurs
2. the name of the plugin and its version. If you are using a pro version - your order number.
3. the version of your WordPress installation
4. copy and paste into the message your system status report. Please read more here: [Instruction on System Status](https://docs.google.com/document/d/1Wi2X8RdRGXk9kMszQy1xItJrpN0ncXgioH935MaBKtc/edit)

== Screenshots ==

1. DB Manager Settings page.

== Changelog ==

= V1.1.2 - 4.05.2017 =
* Bugfix : The bug with PhpMyAdmin installation has been fixed.

= V1.1.1 - 12.10.2016 =
* Update : BWS plugins section is updated.

= V1.1.0 - 12.07.2016 =
* Update : BWS panel section is updated.

= V1.0.9 - 23.03.2016 =
* Bugfix : The bug with user authorization has been fixed.

= V1.0.8 - 04.11.2015 =
* Update : Textdomain was changed.
* Update : We updated all functionality for wordpress 4.3.1.

= V1.0.7 - 06.08.2015 =
* Bugfix : Problem with downloading PhpMyAdmin was fixed.

= V1.0.6 - 20.05.2015 =
* Bugfix: We fixed access to PhpMyAdmin and Dumper for HTTPS-sites.
* Bugfix : We fixed access to PhpMyAdmin and Dumper with a nonstandard title of the plugin folder.
* Update : We updated all functionality for wordpress 4.2.2.

= V1.0.5 - 01.04.2015 =
* Update : BWS plugins section is updated.
* Update : We updated all functionality for wordpress 4.1.1.

= V1.0.4 - 06.01.2015 =
* Update : BWS plugins section is updated.

= V1.0.3 - 19.09.2014 =
* Update : We updated all functionality for wordpress 4.0.
* Bugfix : Security Exploit was fixed.

= V1.0.2 - 20.06.2014 =
* Bugfix : Problem with downloading PhpMyAdmin was fixed.
* Bugfix : Problem with disk qouta warning was fixed.
* Bugfix : Problem with getting configurations from wp-config was fixed.

= V1.0.1 - 11.04.2014 =
* Update : We updated all functionality for wordpress 3.9.1.
* Bugfix : Error with warning about disk quota exceeded was fixed.

= V1.0.0 - 11.04.2014 =
* Bugfix : The code refactoring was performed.
* NEW : Css-style was added.

== Upgrade Notice ==

= V1.1.2 =
* Bugs fixed.

= V1.1.1 =
* Plugin optimization completed.

= V1.1.0 =
BWS panel section is updated.

= V1.0.9 =
The bug with user authorization has been fixed.

= V1.0.8 =
Textdomain was changed. We updated all functionality for wordpress 4.3.1.

= V1.0.7 =
Problem with downloading PhpMyAdmin was fixed.

= V1.0.6 =
We fixed access to PhpMyAdmin and Dumper for HTTPS-sites. We fixed access to PhpMyAdmin and Dumper with a nonstandard title of the plugin folder. We updated all functionality for wordpress 4.2.2.

= V1.0.5 =
BWS plugins section is updated. We updated all functionality for wordpress 4.1.1.

= V1.0.4 =
BWS plugins section is updated.

= V1.0.3 =
We updated all functionality for wordpress 4.0. Security Exploit was fixed.

= V1.0.2 =
Problem with downloading PhpMyAdmin was fixed. Problem with disk qouta warning was fixed. Problem with getting configurations from wp-config was fixed.

= V1.0.1 =
We updated all functionality for wordpress 3.9.1. Error with warning about disk quota exceeded was fixed.

= V1.0.0 =
The code refactoring. Css-style was added.
